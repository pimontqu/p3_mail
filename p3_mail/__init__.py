import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class p3_mail():
    def __init__(self, server_stmp = 'smtp.univ-lille.fr'):
        self.server = smtplib.SMTP()
        # server.set_debuglevel(1) # Décommenter pour activer le debug
        self.server.connect(server_stmp)
    
    def send(self, html_message = '', to_addr = ['quentin.pimont@utc.fr'], from_addr = 'Quentin <quentin.pimont@univ-lille.fr>',  subject = 'Carré magique'):
        fromaddr = from_addr
        toaddrs = to_addr
        sujet = subject
        html = u"""\
        <html>
        <head>
        <meta charset="utf-8" />
        <style>
        table{
        width: 100%;
        border-collapse: collapse;
        }
        </style>
        </head>
        <body>""" + html_message + """
        </body
        </html>
        """
        
        msg = MIMEMultipart('alternative')
        msg['Subject'] = sujet
        msg['From'] = fromaddr
        msg['To'] = ','.join(toaddrs)
        
        part = MIMEText(html, 'html')
        msg.attach(part)
        try:
            self.server.sendmail(fromaddr, toaddrs, msg.as_string())
        except smtplib.SMTPException as e:
            print(e)
        # {} # Réponse du serveur
        self.server.quit()
        self.make_finish_file()

    def make_finish_file(self):
        finish_file = open('/home/p3-scripts/file_zeppelin/finish.txt', 'w')