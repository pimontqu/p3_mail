from setuptools import setup


setup(name='p3_mail',
      version='0.1',
      description='Moodle Api webSocket',
      url='https://gitlab.utc.fr/pimontqu/moodle_p3_api',
      author='Quentin Pimont',
      author_email='quentin.pimont@utc.fr',
      license='MIT',
      packages=['p3_mail'],
      install_requires=[
          'smtplib',
          'email'
      ],
      zip_safe=False)
